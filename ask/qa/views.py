from django.shortcuts import render, get_object_or_404
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.urls import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse, HttpResponseRedirect

from .models import Question
from .forms import AskForm, AnswerForm, SignupForm, LoginForm

def paginator(query, request):
    page = request.GET.get('page', 1)
    paginator = Paginator(query, 10)
    
    try:
        page = paginator.page(page)
    except PageNotAnInteger:
        page = paginator.page(1)
    except EmptyPage:
        page = paginator.page(paginator.num_pages)
    return page
    
# Create your views here.
def test(request, *args, **kwargs):
    return HttpResponse('OK!')

def signup(request):
    error = ''
    if (request.method == "POST"):
        username = request.POST.get('username')
        password = request.POST.get('password')
        email = request.POST.get('email')
        user = User.objects.create_user(
            username,
            email,
            password)
        if user is not None:
            login(request, user)
            return HttpResponseRedirect(reverse('index'))
        else:
            error = 'Create user error'
            signup_form = SignupForm(request.POST)
    else:
        signup_form = SignupForm()

    return render(request, 'qa/signup.html', {
        'form': signup_form,
        'error': error
    })
    
def loginv(request):
    error = ''
    if (request.method == "POST"):
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        
        if user is not None:
            login(request, user)
            return HttpResponseRedirect(reverse('index'))
        else:
            error = 'Login/email incorrect :('
            login_form = LoginForm(request.POST)
    else:
        login_form = LoginForm()

    return render(request, 'qa/login.html', {
        'form': login_form,
        'error': error
    })

def index(request):
    questions = Question.objects.new()
    page = paginator(questions, request)
    page.baseurl = reverse('index') + '?='

    return render(request, 'qa/index.html', {
        'questions': page.object_list,
        'paginator': page.paginator,
        'page': page
    })

def popular(request):
    questions = Question.objects.popular()
    page = paginator(questions, request)
    page.baseurl = reverse('popular') + '?='

    return render(request, 'qa/index.html', {
        'questions': page.object_list,
        'paginator': page.paginator,
        'page': page
    })
    
def ask(request):
    if (request.method == "POST"):
        question_form = AskForm(request.POST)
        if question_form.is_valid():
            question_form._user = request.user
            question = question_form.save()
            url = question.get_url()
            return HttpResponseRedirect(url)
    else:
        question_form = AskForm()

    return render(request, 'qa/ask.html', {
        'form': question_form
    })
    
def question(request, question_id):
    question = get_object_or_404(Question, id = question_id)
    
    if (request.method == "POST"):
        answer_form = AnswerForm(request.POST)
        if answer_form.is_valid():
            answer_form._user = request.user
            answer = answer_form.save()
            url = question.get_url()
            return HttpResponseRedirect(url)
    else:
        answer_form = AnswerForm(initial={'question': question_id})

    return render(request, 'qa/question.html', {
        'question': question,
        'answers': question.answer_set.all(),
        'form': answer_form
    })
