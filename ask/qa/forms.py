from django import forms
from .models import Question, Answer

class SignupForm(forms.Form):
    username = forms.CharField(label='Login',max_length=80)
    email = forms.EmailField(label='Email')
    password = forms.CharField(label='Password',widget=forms.PasswordInput())
    
class LoginForm(forms.Form):
    username = forms.CharField(label='Login',max_length=80)
    password = forms.CharField(label='Password',widget=forms.PasswordInput())

class AskForm(forms.Form):
    title = forms.CharField(max_length=80)
    text = forms.CharField(widget=forms.Textarea)
    
    def clean_title(self):
        title = self.cleaned_data['title']
        if len(title) < 1:
            raise forms.ValidationError('Title is meaningless', code = 1)
        return title
    
    def clean_text(self):
        text = self.cleaned_data['text']
        if len(text) < 1:
            raise forms.ValidationError('Question is too short', code = 2)
        return text
        
    def save(self):
        question = Question(**self.cleaned_data)
        question.author = self._user
        question.save()
        return question

class AnswerForm(forms.Form):
    text = forms.CharField(label='Your anwser', widget=forms.Textarea)
    question = forms.ModelChoiceField(queryset=Question.objects.all(), 
                                    widget=forms.HiddenInput())
    
    def clean_text(self):
        text = self.cleaned_data['text']
        if len(text) < 1:
            raise forms.ValidationError('Answer is too short', code = 2)
        return text
        
    def save(self):
        answer = Answer(**self.cleaned_data)
        answer.author = self._user
        answer.save()
        return answer
