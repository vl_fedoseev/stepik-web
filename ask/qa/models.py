from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User

# Create your models here.
class QuestionManager(models.Manager):
    def new(self):
        return self.order_by('-id')
    def popular(self):
        return self.order_by('-rating')

class Question(models.Model):
    title = models.CharField(max_length=255)
    text = models.TextField(max_length=1024)
    added_at = models.DateTimeField(blank=True, auto_now_add=True)
    rating = models.IntegerField(default=0)
    author = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True)
    likes = models.ManyToManyField(User, related_name='likes_set')
    
    objects = QuestionManager()
    
    def get_url(self):
        return reverse('question', 
            kwargs={'question_id': self.id})

    def __str__(self):
        return self.title
    
class Answer(models.Model):
    text = models.TextField()
    added_at = models.DateTimeField(blank=True, auto_now_add=True)
    question = models.ForeignKey(Question, on_delete=models.CASCADE, blank=True, null=True)
    author = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True) 
    
    def __str__(self):
        return self.text
