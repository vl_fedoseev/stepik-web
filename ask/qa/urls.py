from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^login/', views.loginv, name='login'),
    url(r'^signup/', views.signup, name='signup'),
    url(r'^question/(?P<question_id>[0-9,a-z]+)/$', views.question, name='question'),
    url(r'^ask/', views.ask, name='ask'),
    url(r'^popular/', views.popular, name='popular'),
]
