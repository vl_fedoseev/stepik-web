#!/bin/bash
case $DOCKER_ENV in
    "nginx")
       ln -s -f /home/box/web/etc/nginx.conf /etc/nginx/conf.d/test.conf
       rm -f /etc/nginx/sites-enabled/default
       rm -f /etc/nginx/conf.d/default.conf
       /etc/init.d/nginx restart
        ;;
    "gunicorn")
        echo "GUNICORN"
        gunicorn  -c /etc/gunicorn.d/hello.py hello:app
        cd ask
        python manage.py makemigrations qa
        python manage.py migrate
        gunicorn  -c /etc/gunicorn.d/ask.py ask.wsgi:application
        ;;
      
        
esac
