def app(environ, start_response):
        q = environ['QUERY_STRING']
        response = "\n".join(q.split("&"))
        response_body = bytes(response, encoding= 'utf-8')
        start_response("200 OK", [
            ("Content-Type", "text/plain"),
            ("Content-Length", str(len(response_body)))
        ])
        return iter([response_body])
