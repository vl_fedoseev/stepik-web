#!/bin/bash
sudo /etc/init.d/mysql start
mysql -u root -e 'create database qa'
mysql -u root -e 'create user "qa_app" identified by "stepik"'
mysql -u root -e 'grant all on qa.* to "qa_app"'


sudo ln -s -f /home/box/web/etc/hello.py /etc/gunicorn.d/hello.py
sudo ln -s -f /home/box/web/etc/ask.py /etc/gunicorn.d/ask.py
sudo gunicorn --daemon -c /etc/gunicorn.d/hello.py hello:app
cd ask
python3 manage.py makemigrations qa
python3 manage.py migrate
sudo gunicorn --daemon -c /etc/gunicorn.d/ask.py ask.wsgi:application

sudo ln -s /home/box/web/etc/nginx.conf /etc/nginx/conf.d/test.conf
sudo rm -f /etc/nginx/sites-enabled/default
sudo rm -f /etc/nginx/conf.d/default.conf
sudo /etc/init.d/nginx restart
